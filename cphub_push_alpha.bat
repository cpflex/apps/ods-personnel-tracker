@ECHO OFF
if [%1]==[] goto usage

set specfile="v1_spec.json"
set tag=%1
ECHO Pushing ODS Personnel Traacker Application version %1
cphub push -v -s %specfile% cpflexapp "./ods-personnel-tracker.bin" cora/apps/tracking/ods-personnel-tracker:%tag%
goto :eof
:usage
@echo Usage: %0 ^<tag (e.g. v1.0.2.3)^>
exit /B 1
