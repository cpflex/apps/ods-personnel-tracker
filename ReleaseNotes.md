﻿# ODS Personnel Tracker Application Release Notes

### V1.010a - 240111
1. Replaced Motion Tracking with TrackingEngine State Machine implementation with
   the documented state machine model supporting three different operating modes (normal, duress, and extended duress modes).
2. Updated CT1XXX firmware reference to v2.5.0.0a
3. Platform Reference v1.4.5.1

### V1.009 - 231219
1. Bugfix: 5 clicks should initiate immediate located.
2. Falling back to firmware v2.3.0.0, latest has issues.
3. Changed default positioning mode to precise to help with noisy BLE environments.

### V1.0042a - 231219
1.  Firmware reference v2.3.1.0a
  -Fixes OPL BLE Only scan issues (KP-321)
 - Location module positioning technology default changed from AUTOMATIC to BLE


### V1.006a - 231209
1. Updated CT1XXX firmware reference to v2.4.4.2a
 - Fixed TimeSync bug (KP-350)
 - Various RTC Kernel Fixes
 - NETWORK RESET operation now rejoins instead of clearing NVM params
 - DUX12 accelerometer bug fixes
2. LoraWAN v1.04 radio stack
  - US915 SB2 / ADR Disabled
  - EU868 Standard Band / ADR enabled

### V1.004 - 230919
1. Firmware Reference v2.3.0.0
   * Incorporates all stabilization improvements.  This is a nearly final release candidate for the firmware.
2. Flex Platform: v1.4.0.2
3. Standard EU band ADR enabled.

### V1.003 - 2030818
  1. Downlinked state changes for duress and notify 1 will trigger uplink notifications.
  2. bugfix:  Downlink command state changes for duress and notify1 now work  again.   

### V1.002  - 230814
  1. Set default precision mode to medium quality.
  2. Set network polling mode to 2 minutes when in duress mode.
   
### V1.001 - 230808
  1. Updated Platform  v1.3.2.2
     * Bugfix:  fixed duplicate state issue.

### V1.000a - 230804
  1. Firmware Reference  v2.2.1.0
  2. Initial Implementation
