/**
*  Name:  ConfigSystem.p
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for the battery module.
**/

/*************************
* Battery Configuration 
**************************/
#include <StatefulEventDefs.i>
#include <OcmNidDefinitions.i>

const SE_DEF_STATECOUNT = 2;

//FIELDS ARE nidMsg, flags, nidSet, nidClear, priority, category, initialState
stock SE_EVENTDEFS[SE_DEF_STATECOUNT][.nidMsg, .flags, .nidSet, .nidClear, .priority, .category, .initialState] = [
	//Emergency Mode.
	[ 
		_: NID_OdsEventDuress, 
		_: (SE_CONFIG_PERSIST_STATE | SE_CONFIG_RPT_DOWNLINK), 
		_: NID_OdsEventDuressActivated, 
		_: NID_OdsEventDuressDeactivated, 
		_: MP_critical,
		_: MC_alert,
		_: SE_CLEAR
	], [ 
		_: NID_OdsEventNotify1, 
		_: (SE_CONFIG_TRANSIENT | SE_CONFIG_RPT_DOWNLINK), //Does not send state information.
		_: 0, 
		_: 0, 
		_: MP_med, 
		_: MC_info,
		_: SE_UNKNOWN		
	] 
];


