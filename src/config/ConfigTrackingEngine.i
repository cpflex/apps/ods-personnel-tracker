/**
*  Name:  ConfigTrackingEngine.i
*
*  This module is the proprietary property of Codepoint Technologies, Inc.
*  Copyright (C) 2023 Codepoint Technologies, Inc.
*  All Rights Reserved
*
* Description:
*  Configuration constants for network link Checks.
**/

#include "ui.i"
#include "telemetry.i"
#include <OcmNidDefinitions.i>
#include "TrackingEngineDefs.i"


/*******************************************************************************
* Tracking Engine General Configuration
*******************************************************************************/
//NID Command Map, by default these map to ped_tracker protocol spec.
//If your protocol is different, update the NID mappings.

const _: {
	NM_CMD_CONFIG	= _: NID_TrackConfig,
	NM_INACTIVITY	= _: NID_TrackConfigInactivity,
	NM_NOMINTVL 	= _: NID_TrackConfigNomintvl,
	NM_EMRINTVL		= _: NID_TrackConfigEmrintvl,
	
	NM_CMD_TRACKING	= _: NID_TrackMode,
	NM_ACTIVE		= _: NID_TrackModeActive,
	NM_ACQUIRE		= _: NID_TrackConfigAcquire,
	NM_ENABLE		= _: NID_TrackModeEnabled,
	NM_DISABLE		= _: NID_TrackModeDisabled	
};

const bool:     TRACKING_INIT_ACTIVATE = true;  // If true, tracking is initially active.

// Acquire positioning data indicator
const 			TRACKING_IND_LED_ACTIVATE    = LED1;
const 			TRACKING_IND_LED_DEACTIVATE  = LED2;
const 			TRACKING_IND_COUNT = 4;
const  bool:    TRACKING_IND_ENABLE = false;	//Tracking activation / deactivation 
                                                //indicator is disabled, since always active.
const  bool:	ACQUIRE_IND_ENABLE	= true;		//Acquire indicator Enabled
const			ACQUIRE_IND_LED		= LED1;		//LED to use for indication.
const			ACQUIRE_IND_COUNT	= 2;		//Number of cycles to blink.
const			ACQUIRE_IND_ON		= 466;		//Blink on-time in milliseconds.
const			ACQUIRE_IND_OFF		= 133;		//Blink off-time in milliseconds.

// Persisted variable default values.
//These values define the initial value for persisted configurations.
//These are set when the application is first run after installation.
const DEFAULT_ACQUIRE = 50;		//Defines the movement threshold (mg) to begin acquisition.
const DEFAULT_INACTIVITY = 100;     //Defines movement threshold (mg) to test for innactivity
const DEFAULT_INTVL_NORMAL = 1;     //Defines the reporting interval in minutes
const DEFAULT_INTVL_EMERGENCY = 5;   //Defines the reporting interval in minutes

const  TelemPostFlags: DEFAULT_FLAGS_POSTACQUIRE = TPF_ARCHIVE;

//Uncomment to turn on debugging in Tracking Engine.
//const TRK_DEBUG = 1;

/*******************************************************************************
* Tracking Engine State and Properties
*******************************************************************************/

/*******************************
* @brief Enum defines the motion tracking states
********************************/
const TE_STATEDEFS_COUNT = 10;
const  {
	TE_NormalMode=0,
    TE_NormalWaitMotion = 1,
    TE_NormalLocate = 2,     // Normal Mode Tracking, Locate on Enter
    TE_NormalWaiting = 3,    // Normal Mode Waiting Prop0 (default 300 seconds)
    TE_DuressMode = 4,
    TE_DuressLocate = 5,     // Duress Mode Tracking, Locate on Enter
    TE_DuressWaiting = 6,    // Duress Mode Waiting Prop1 (default 30 seconds)
    TE_ExtDuressMode = 7,
    TE_ExtDuressLocate = 8,  // Extended Duress Mode Tracking, Locate on Enter
    TE_ExtDuressWaiting = 9  // Extended Duress Mode Waiting Prop2 (default 120 seconds)
};

/*******************************
* @brief Enum User friendly names of the state.
********************************/
stock const  TE_StateNames[TE_STATEDEFS_COUNT]{4} = [ 
	"NMM", "IDL", "NML", "NMW", "DRM", "DRL", "DRW", "EXM", "EXL", "EXW"
];

/*******************************
* @brief Enum defines Properties define in the Tracking engine.
********************************/
const TE_PROPERTIES_COUNT=5; 
const {
    TEPROP_NMW_INTERVAL = 0,   //Normal Mode Locate Interval while in motion.
    TEPROP_DRW_INTERVAL = 1,   //Duress Mode Locate Interval while in motion.
    TEPROP_EXW_INTERVAL = 2,   //Ext. Duress Mode Locate Interval while in motion.
    TEPROP_DRW_TIMEOUT = 3,    // Duress Mode Timeout.
    TEPROP_EXW_TIMEOUT = 4,    // Extended Duress Mode Timeout.
};

//*******************************
// @brief 1-D array defines properties array (numerically indexed starting at 0).
// @remarks A maximum of 15 properties may be defined.
//*******************************
stock const TE_PROPERTIES_DEFAULT [TE_PROPERTIES_COUNT] = [
    300,  //Normal Mode Locate Interval Default in seconds 
    30,   //Duress Mode Locate Interval Default in seconds 
    120,  //Extended Duress Mode Locate Interval Default in seconds 
    600,  //Duress Mode Wait Timeout in seconds.
    14400 //Extended Duress Mode Wait Timeout in seconds.
];

/*******************************************************************************
* Tracking Engine State Definitions
*******************************************************************************/

/*******************************
* @brief 2-D array defines the tracking state machine.
********************************/
stock const TE_STATEDEFS [TE_STATEDEFS_COUNT] [
    .flags,  //General State configuration flags.
    .track,  //Tracking configuration flags.
    .motion,  //Motion detection configuration flags.
    .report  //Reporting configuration flags.
] = [

    //--------------------------------------
    // Normal Mode States
    //--------------------------------------

    //STATE TE_NormalMode - //Enter Normal Mode. Reset User2 Mode Entry Time
    [
        TESF_CallbackOnEnter,  
        0,
        0, 
        0
    ], 

    //STATE TE_NormalWaitMotion - //Wait for any Motion to activate Normal Mode.
    [
        TESF_CallbackUser1,  
        0,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100), 
        0
    ], 

    //STATE TE_NormalLocate - //Execute a locate on Enter 
    [
        0,  
        TETF_LocateOnEnter, 
        0, // Motion sensing disabled during this state
        TERPT_Enabled | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ], 

    //STATE TE_NormalWaiting 
    [
        TESF_SAMPLEINTERVAL_SET(0xFFF0) | TESF_CallbackOnAcqComplete | TESF_CallbackUser1,  //Map interval to property 0.
        TETF_Enabled,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ],     

    //--------------------------------------
    // Duress Mode States
    //--------------------------------------

    //STATE TE_DuressMode - //Enter Duress Mode. Reset User2 Mode Entry Time
    [
        TESF_CallbackOnEnter,  
        0,
        0, 
        0
    ], 

    //STATE TE_DuressLocate - //Execute a locate on Enter 
    [
        0,  
        TETF_LocateOnEnter, 
        0, // Motion sensing disabled during this state
        TERPT_Enabled | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ], 

    //STATE TE_DuressWaiting 
    [
        TESF_SAMPLEINTERVAL_SET(5) | TESF_CallbackOnAcqComplete | TESF_CallbackUser1,  //Map interval to property 0.
        TETF_Enabled,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled |  TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ],  

    //--------------------------------------
    // ExtDuress Mode States
    //--------------------------------------

    //STATE TE_ExtDuressMode - //Enter Extended Duress Mode. Reset User2 Mode Entry Time
    [
        TESF_CallbackOnEnter,  
        0,
        0, 
        0
    ], 

    //STATE TE_ExtDuressLocate - //Execute a locate on Enter 
    [
        0,  
        TETF_LocateOnEnter | TESF_CallbackOnEnter,   //Perform locate and callback on Enter.
        0, // Motion sensing disabled during this state
        TERPT_Enabled | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ], 

    //STATE TE_ExtDuressWaiting 
    [
        TESF_SAMPLEINTERVAL_SET(5) | TESF_CallbackOnAcqComplete | TESF_CallbackUser1,  //Map interval to property 0.
        TETF_Enabled,
        TEMF_Enabled | TEMF_ProfileWakeup | TEMF_ResNormal | TEMF_Sampling1Hz | TEMF_DURATION_SET(1) | TEMF_THRESHOLD_SET(100),
        TERPT_Enabled | TERPT_POST_SET( TPF_NONE) | TERPT_PRIORITY_SET(MP_med) 
    ],  
];


/*******************************************************************************
* Tracking Engine Trigger Definitions
*******************************************************************************/

const TE_TRIGGERDEFS_COUNT = 14;
stock const TE_TRIGGERDEFS [TE_TRIGGERDEFS_COUNT][
    .flags,
    .motion, 
    .duration, 
] = [
 
    /*****************************************************
    * Normalize Mode States Trigger Definitions
    *****************************************************/
    //---------------------------------------------------
    // TE_NormalMode Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_NormalMode ) |  TETRIG_IDXNEXT_SET( TE_NormalWaitMotion) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],

    //---------------------------------------------------
    // TE_NormalWaitMotion Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET(TE_NormalWaitMotion ) |  TETRIG_IDXNEXT_SET( TE_NormalLocate) |  TETRIG_SIGNALS_SET( TESIG_MOTION),
        0,
        0,
    ],


    //---------------------------------------------------
    // TE_NormalLocate Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_NormalLocate ) |  TETRIG_IDXNEXT_SET( TE_NormalWaiting) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],
    
    //---------------------------------------------------
    // TE_NormalWaiting Triggers
    //---------------------------------------------------

    // TRIGGER On TESIG_USER1
    //   transition to TE_DuressLocate -- if User1 = Duress event.
    [
        TETRIG_IDXSTATE_SET( TE_NormalWaiting ) |  TETRIG_IDXNEXT_SET( TE_DuressMode) |  TETRIG_SIGNALS_SET( TESIG_USER1) | TETRIG_USER1_EVAL,
        0,
        0
    ],  

    // TRIGGER On Sample Interval
    //   if significant motion detected within the specified sample interval, 
    //   transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_NormalWaiting ) |  TETRIG_IDXNEXT_SET( TE_NormalLocate) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        TEME_Enabled | TEME_MINTICKS_SET(2) | TEME_MAXTICKS_SET( IGNORE_VALUE),
        TESE_Enabled | TESE_MINTICKS_SET(0xFFF0) | TESE_MAXTICKS_SET( IGNORE_VALUE)
    ],

    // TRIGGER On Sample Interval
    //   If no motion detected in the sample interval, go back to WOM. 
    [
        TETRIG_IDXSTATE_SET( TE_NormalWaiting ) |  TETRIG_IDXNEXT_SET( TE_NormalWaitMotion) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        0,
        TESE_Enabled | TESE_MINTICKS_SET(0xFFF0) | TESE_MAXTICKS_SET( IGNORE_VALUE)
    ],



    /*****************************************************
    * Duress Mode States Trigger Definitions
    *****************************************************/
    //---------------------------------------------------
    // TE_DuressMode Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_DuressMode ) |  TETRIG_IDXNEXT_SET( TE_DuressLocate) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],

    //---------------------------------------------------
    // TE_DuressLocate Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_DuressLocate ) |  TETRIG_IDXNEXT_SET( TE_DuressWaiting) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],
    
    //---------------------------------------------------
    // TE_DuressWaiting Triggers
    //---------------------------------------------------

    // TRIGGER On Sample Interval
    //   if any motion detected within the specified sample interval, 
    //   transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_DuressWaiting ) |  TETRIG_IDXNEXT_SET( TE_DuressLocate) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( IGNORE_VALUE),
        TESE_Enabled | TESE_MINTICKS_SET(0xFFF1) | TESE_MAXTICKS_SET( IGNORE_VALUE)
    ],

    //TRIGGER on AcqComplete is handled by the TESF_CallbackOnAcqComplete handler.
    //If number of measurements is 0 and we have tried < 2 times, Set next State TE_DuressLocate

    //TRIGGERS ON USER1 is handled by TCBE_CallbackUser1, which will set the state directly.

    // TRIGGER On Sample Interval 
    //   transition to TE_ExtDuressLocate -- if User2 > Prop3
    [
        TETRIG_IDXSTATE_SET( TE_DuressWaiting  ) |  TETRIG_IDXNEXT_SET( TE_ExtDuressMode) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE) | TETRIG_USER2_EVAL,
        0,
        0,
    ],  

   /*****************************************************
    * ExtDuress Mode States Trigger Definitions
    *****************************************************/
    //---------------------------------------------------
    // TE_ExtDuressMode Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_ExtDuressMode ) |  TETRIG_IDXNEXT_SET( TE_ExtDuressLocate) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],

    //---------------------------------------------------
    // TE_ExtDuressLocate Triggers
    //---------------------------------------------------

    // TRIGGER ON Enter State, Transition to next State
    [
        TETRIG_IDXSTATE_SET( TE_ExtDuressLocate ) |  TETRIG_IDXNEXT_SET( TE_ExtDuressWaiting) |  TETRIG_SIGNALS_SET( TESIG_ENTER_STATE),
        0,
        0,
    ],
    
    //---------------------------------------------------
    // TE_ExtDuressWaiting Triggers
    //---------------------------------------------------

    // TRIGGER On Sample Interval
    //   if any motion detected within the specified sample interval, 
    //   transition to tracking locate, 
    [
        TETRIG_IDXSTATE_SET( TE_ExtDuressWaiting ) |  TETRIG_IDXNEXT_SET( TE_ExtDuressLocate) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE),
        TEME_Enabled | TEME_MINTICKS_SET(1) | TEME_MAXTICKS_SET( IGNORE_VALUE),
        TESE_Enabled | TESE_MINTICKS_SET(0xFFF2) | TESE_MAXTICKS_SET( IGNORE_VALUE)
    ],

    //TRIGGER on AcqComplete is handled by the TCBE_SignalAcqComplete handler.
    //If number of measurements is 0 and we have tried < 2 times, Set next State TE_DuressLocate

    //TRIGGERS ON USER1 is handled by TCBE_CallbackUser1, which will set the state directly.

    // TRIGGER On Sample Interval 
    //   transition to TE_NormalMOde -- if User2 > Prop4
    [
        TETRIG_IDXSTATE_SET( TE_ExtDuressWaiting  ) |  TETRIG_IDXNEXT_SET( TE_NormalMode) |  TETRIG_SIGNALS_SET( TESIG_SAMPLE) | TETRIG_USER2_EVAL,
        0,
        0,
    ],  
]; 

