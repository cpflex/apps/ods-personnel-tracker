/*******************************************************************************
*                !!! OCM CODE GENERATED FILE -- DO NOT EDIT !!!
*
* ODS Personnel Tracker Protocol Schema
*  nid:        OdsPersonnelTracker
*  uuid:       e4b7b6e6-ab85-4def-b682-8b2c39c19fe1
*  ver:        1.0.0.0
*  date:       2023-08-04T23:11:17.128Z
*  product: Cora Tracking CT100X
* 
*  Copyright 2023 - Codepoint Technologies, Inc. All Rights Reserved.
* 
*******************************************************************************/
const OcmNidDefinitions: {
	NID_LocConfigPmode = 1,
	NID_SystemLogAll = 2,
	NID_LocConfigPmodeDefault = 3,
	NID_TrackConfig = 4,
	NID_OdsEventDuress = 5,
	NID_SystemLogDisable = 6,
	NID_PowerBattery = 7,
	NID_LocConfigPtech = 8,
	NID_SystemReset = 9,
	NID_PowerChargerCritical = 10,
	NID_TrackConfigNomintvl = 11,
	NID_SystemConfigPollintvl = 12,
	NID_SystemLog = 13,
	NID_TrackMode = 14,
	NID_SystemLogAlert = 15,
	NID_LocConfigPmodePrecise = 16,
	NID_LocConfigPtechBle = 17,
	NID_LocConfigPtechWifi = 18,
	NID_TrackModeActive = 19,
	NID_LocConfig = 20,
	NID_LocConfigPmodeMedium = 21,
	NID_PowerCharger = 22,
	NID_LocConfigPmodeCoarse = 23,
	NID_OdsEventNotify1 = 24,
	NID_TrackModeDisabled = 25,
	NID_TrackConfigEmrintvl = 26,
	NID_SystemErase = 27,
	NID_LocConfigPtechWifible = 28,
	NID_LocConfigPtechAutomatic = 29,
	NID_PowerChargerCharging = 30,
	NID_OdsEventDuressActivated = 31,
	NID_SystemLogInfo = 32,
	NID_TrackConfigAcquire = 33,
	NID_TrackConfigInactivity = 34,
	NID_OdsEventDuressDeactivated = 35,
	NID_SystemLogDetail = 36,
	NID_PowerChargerCharged = 37,
	NID_SystemConfig = 38,
	NID_PowerChargerDischarging = 39,
	NID_TrackModeEnabled = 40,
	NID_OdsEvent = 41
};
