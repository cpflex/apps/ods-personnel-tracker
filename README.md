# ODS Personnel Tracker Application
**Version: 1.010a**<br/>
**Release Date: 2024/01/11**

This application provides personnel tracking and emergency notification functions 
for the ODS Application.

Click here for [Release Notes](./ReleaseNotes.md).

## Key Features  
The tracker application is designed to provide useful tracking and event 
information as an individual goes about their daily activities supporting two types 
of notifications.

Key features include:

1. Single-click emergency activation and location report
2. Three (3) second "press and hold" secondary notification and location report.
3. Motion activated location data collection
4. Cloud configurable location and tracking settings / report
5. User/Cloud controlled tracking activation / deactivation
6. Caching of messages when out of range
7. Battery status events and report
8. Location measurement configuration control for admin

# User Interface 
This section describes the essential functions of the CT1000 user interface as defined
by this application.   

## Button Commands  

The following tables summarize the button activated commands organized into primary operational and additional commands.  
### Operational Commands 
These commands are intended for operational use, providing a very simple user interface.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Emergency Activation**   | single (1) <br>quick press        | any button or both       | Activates Emergency state and sends locate notification, can only be terminated by cloud. See [*Emergency Mode*](#emergency-mode) |  
| **Secondary Notification**   | 3 Quick Presses        | any button or both       | Sends secondary notification event and location report  |  

### Additional Commands
For testing and device management, the following commands are available but not required
for ordinary operational use.
| **Command**                         |     **Action**       |     **Button**     |  **Description**           |
|-------------------------------------|:--------------------:|:------------------:|---------------------------|
| **Locate Now** <br> NOT AVAILABLE  | five (5) <br> quick presses | Button #1       | Sends location report.  | 
| **Battery Status**                  | five (5) <br>quick presses        | Button #2       | Indicates battery level <br> [*See Battery Indicator*](#battery-indicator) 
| **Deactivate Emergency**<br>*Development Only*    | seven (7) <br> quick presses | any button or both   | Deactivates emergency state and sends deactivation notice.  This is a development feature only and will be removed once system is fully operational. | 
| **Network Reset**                   | hold > 15<br>seconds              | any button or both | Network reset device      |
| **Factory Reset**                   | hold > 25<br>seconds              | any button or both | Resets the network and configuration to factory defaults for the application.     |
 
## LED Descriptions

The following table indicates the meaning of the two bi-color LEDs on the device.  The LED's are
positioned:

* **LED #1** - Upper LED, furthest away from charger pads
* **LED #2** - Lower LED, closest to charger pads 

| **Description**                |                **Indication**               |   **LEDS**  |
|--------------------------------|:-------------------------------------------:|:-----------:|
| **Emergency Mode Activating**  |  Red blink <br>ten (10) times quickly       |    both     |
| **Emergency Mode Active**      |  Two (2) red blink once every 5 seconds     |    both     |
| **Emergency Mode Deactivating** |  Green blink <br>ten (10) times quickly    |    both     |
| **Device Reset**               |       Green blink <br>three (3) times       |     both    |
| **Locate Initiated**           |        Green blink <br>two (2) times        |    LED #1   |
| **Battery Charging**           | Orange slow blink every<br>ten (10) seconds |    LED #1   |
| **Battery Fully Charged**      |  Green slow blink every<br>ten (10) seconds |    LED #1   |

## Emergency Mode 

Emergency mode provides a panic button feature, where when the user clicks any button the emergency mode is activated.  Once activated it sends an notification message to the cloud and continuously tracks tracks the user. The specified emergency tracking interval is one (1) minutes by default but can be modified by the cloud services.

The emergency mode can only be deactivated by the cloud.
## Battery Indicator 

Activating the battery status commend will indicate the percent charge of the battery in 20% increments.

LED #2 will blink as follows to indicate percent charge.

| LED #2   | % Charge    | Description|
|----------|-------------|------------|
|  1 red   |   < 5%      | Battery level is critical,  charge immediately. |
|  1 green |  5%  - 30%  | Battery low  less than 30% remaining. |
|  2 green |  30% - 50%  | Battery is about 40% charged. |
|  3 green |  50% - 70%  | Battery is about 60% charged. |
|  4 green |  70% - 90%  | Battery  is about 80% charged. |
|  5 green |  > 90%      | Battery is fully charged. |

The tag will report battery status when connect or disconnect from charging. The battery level and temperature are monitored every 10 minutes. The tag will also report battery status when its level change more than 5%. The tag will go to low power mode and stop automatic location reporting when battery level is below 20%. The tag will resume automatic location reporting after battery 
level return to above 25%. 

### Tracker State Machine Model
The following diagram details the state machine used by the tracker to manage location updates and when duress mode is automatically cancelled.
![picture](design/Duress%20Application%20State%20Machine.png)

* Normal Mode (Green) is when the Card is not in a duress state.   
* Duress Mode (Red) is when the Card is in a Duress or Activated State.  
* Extended Duress (ExtDuress) is when the Card is in a Duress state for more than 10 (default) minutes.   
 
Locate intervals and mode duration are configurable by downlinking one or all of the 5 properties.  Locates are not archived.  Duress events are archived for 4 hours and then discarded.


---
*Copyright 2023-2024, Codepoint Technologies, Inc.* <br>
*All Rights Reserved*
